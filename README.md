# sre-helm

This helm chart will deploy three spring boot applications(backend,frontend & reader) using the image tag generated in https://gitlab.com/pranay142/sre-challenge project.

# Prerequisites:

1. Postgres and Kafka should be deployed in the kubernetes cluster in postgres and kafka namespace respectively.
2. DB Username and password should be configured in the environment variable.
3. kubeconfig file should be configured as an environment variable.
